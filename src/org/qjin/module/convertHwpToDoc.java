package org.qjin.module;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rcc.h2tlib.parser.H2TParser;
import rcc.h2tlib.parser.HWPVER;
import rcc.h2tlib.parser.HWPMeta;
import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.BreakClear;
import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.LineSpacingRule;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.TextAlignment;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.VerticalAlign;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import com.argo.hwp.HwpTextExtractor;

public class convertHwpToDoc {

	public void getHWPfiles() throws Exception{
		String file = "C:\\workspace_hwp\\20022003\\498(090102).hwp";
		String fi = "E:\\test\\119.hwp";
		StringBuilder sb = new StringBuilder();
		HWPMeta meta = new HWPMeta();
		H2TParser parser = new H2TParser();
		
	    boolean flgs = parser.IsHanFile(fi);
	    System.out.println(flgs);
		boolean flg = parser.GetText(file, meta, sb);
		if (flg){
			System.out.println("HWP 체크");
			System.out.println("title => " + meta.getTitle());
			System.out.println("subject => " + meta.getSubject());
			System.out.println("author => " + meta.getAuthor());
			System.out.println("createtime => " + meta.getCreatetime());
			System.out.println("keyword => " + meta.getKeyword());
			System.out.println("comment => " + meta.getComment());
			if(meta.getVer() == HWPVER.HML2) System.out.println("ver => HML2");
			if(meta.getVer() == HWPVER.HWP3) System.out.println("ver => HWP3");
			if(meta.getVer() == HWPVER.HWP5) System.out.println("ver => HWP5");
	        System.out.println(sb.toString());
	        
	 //       writeDocFile(meta.getTitle(), sb);
		}
		else{
		}
	}
	
	public boolean convertHwptoDocx(String path, String subPath, String filename) throws FileNotFoundException, IOException{
		boolean result = false;
		String file = path+filename;
		StringBuilder sb = new StringBuilder();
		HWPMeta meta = new HWPMeta();
		H2TParser parser = new H2TParser();
		boolean flg = false;
		
		File hwp = new File(file); 
	    Writer writer = new StringWriter();
	    HwpTextExtractor.extract(hwp, writer);
	    String text = writer.toString();
	    sb.append(text);

	//	if(parser.IsHanFile(file))
			//flg = parser.GetText(file, meta, sb);
	//	flg = parser.GetMeta(file, meta);
	    flg = true;
		if(flg){
			//String filenum = filename.substring(0, filename.indexOf("."));
			//String newFileName = filenum+" "+meta.getTitle()+".docx";
			try {
				System.out.println("======Argo TEXT: "+text);
				//System.out.println("Version : "+meta.getVer() +" ====TEXT: "+sb.toString());
				writeDocFile(subPath, filename, sb);
				System.out.println(" ================== > file "+filename+" DONE!");
				result = true;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}else{
			System.out.println("file "+filename+" error!");
			result = false;
		}
		return result;
	}
	
	/**
	 * �뚯씪紐낆쓣 諛쏆븘���대떦 �뚯씪紐낆씠 �ъ슜媛�뒫��寃껋씤吏��뺤씤�섎뒗 Method
	 * �뚯씪紐낆쑝濡��ъ슜��遺덇��ν븯硫�蹂�솚�쒕떎.
	 */
	public String getProperFileName(String filename, String title){
		String tempFilename = filename;
	/*	String regexhangul = ".*[ㄱ-ㅎㅏ-ㅣ가-힣]+.*";
		tempFilename = tempFilename.replaceFirst("\\s", "");
		if(tempFilename.length() < 20 || !tempFilename.matches(regexhangul)){
			tempFilename += " "+title;
		}*/
		
		//Cannot use these special charaters {\, /, :, *, ?, ", <, >, |} as a file name
		
		tempFilename = tempFilename.replace("\"", "");
		tempFilename = tempFilename.replace("\\", "_");
		tempFilename = tempFilename.replace(":", "_");
		tempFilename = tempFilename.replace("/", "_");
		tempFilename = tempFilename.replace("<", "(");
		tempFilename = tempFilename.replace(">", ")");
		tempFilename = tempFilename.replace("*", "-");
		tempFilename = tempFilename.replace("|", "_");
		tempFilename = tempFilename.replace("?", " ");
		
		return tempFilename;
	}
	
	public void writeDocFile(String subPath, String title, StringBuilder context) throws Exception{
		XWPFDocument doc = new XWPFDocument();
		XWPFParagraph para1 = doc.createParagraph();
		
		String contextPara = context.toString();
		//MS949 Encoding
		//최목사님 구분 표시자 이상
		contextPara = contextPara.replaceAll("\\?\\+0x2572", " <기타> ");
		contextPara = contextPara.replaceAll("\\?\\+0x2571", " <개인> ");
		contextPara = contextPara.replaceAll("\\?\\+0x2570", " <교회> ");
		//따움표 열음 깨짐기호
		contextPara = contextPara.replaceAll("󰡒", " ");
		//따움표 당음 깨짐기호
		contextPara = contextPara.replaceAll("󰡓", " ");
		//따움표 열음 이상자
		contextPara = contextPara.replaceAll("“", " ");
		//따움표 닫음 이상자
		contextPara = contextPara.replaceAll("”", " ");
		//최목사님 구분 표시자
		contextPara = contextPara.replaceAll("󰁵", "[기타]");
		contextPara = contextPara.replaceAll("󰁴", "[교회]");
		contextPara = contextPara.replaceAll("󰁳", "[개인]");

		
		
		String[] contextArray = contextPara.split("\n");
		
		
		String tempFilename = title + ".docx";
		XWPFRun r1 = para1.createRun();
		for(String text : contextArray){
			r1.setText(text.trim());
			r1.addCarriageReturn();
		}
		/*if(contextArray.length > 0){
			tempFilename = contextArray[0];
			tempFilename = getProperFileName(tempFilename, title)+".docx";
		}*/
		
		String filename = "C:\\workspace_doc\\"+subPath + "\\"+tempFilename;
		FileOutputStream out = new FileOutputStream(filename);
		doc.write(out);
		out.close();
	}
	
	public void writeDocFileSample() throws FileNotFoundException, Exception{
		XWPFDocument doc = new XWPFDocument();
		XWPFParagraph p1 = doc.createParagraph();
        p1.setAlignment(ParagraphAlignment.CENTER);
        p1.setBorderBottom(Borders.DOUBLE);
        p1.setBorderTop(Borders.DOUBLE);

        p1.setBorderRight(Borders.DOUBLE);
        p1.setBorderLeft(Borders.DOUBLE);
        p1.setBorderBetween(Borders.SINGLE);

        p1.setVerticalAlignment(TextAlignment.TOP);

        XWPFRun r1 = p1.createRun();
        r1.setBold(true);
        r1.setText("The quick brown fox");
        r1.setBold(true);
        r1.setFontFamily("Courier");
        r1.setUnderline(UnderlinePatterns.DOT_DOT_DASH);
        r1.setTextPosition(100);

        XWPFParagraph p2 = doc.createParagraph();
        p2.setAlignment(ParagraphAlignment.RIGHT);

        //BORDERS
        p2.setBorderBottom(Borders.DOUBLE);
        p2.setBorderTop(Borders.DOUBLE);
        p2.setBorderRight(Borders.DOUBLE);
        p2.setBorderLeft(Borders.DOUBLE);
        p2.setBorderBetween(Borders.SINGLE);

        XWPFRun r2 = p2.createRun();
        r2.setText("jumped over the lazy dog");
        r2.setStrike(true);
        r2.setFontSize(20);

        XWPFRun r3 = p2.createRun();
        r3.setText("and went away");
        r3.setStrike(true);
        r3.setFontSize(20);
        r3.setSubscript(VerticalAlign.SUPERSCRIPT);


        XWPFParagraph p3 = doc.createParagraph();
        p3.setWordWrap(true);
        p3.setPageBreak(true);
                
        //p3.setAlignment(ParagraphAlignment.DISTRIBUTE);
        p3.setAlignment(ParagraphAlignment.BOTH);
        p3.setSpacingLineRule(LineSpacingRule.EXACT);

        p3.setIndentationFirstLine(600);
        

        XWPFRun r4 = p3.createRun();
        r4.setTextPosition(20);
        r4.setText("To be, or not to be: that is the question: \n"
                + "Whether 'tis nobler in the mind to suffer \r\n"
                + "The slings and arrows of outrageous fortune, "
                + "Or to take arms against a sea of troubles, "
                + "And by opposing end them? To die: to sleep; ");
        r4.addBreak(BreakType.PAGE);
        r4.setText("No more; and by a sleep to say we end "
                + "The heart-ache and the thousand natural shocks "
                + "That flesh is heir to, 'tis a consummation "
                + "Devoutly to be wish'd. To die, to sleep; "
                + "To sleep: perchance to dream: ay, there's the rub; "
                + ".......");
        r4.setItalic(true);
//This would imply that this break shall be treated as a simple line break, and break the line after that word:

        XWPFRun r5 = p3.createRun();
        r5.setTextPosition(-10);
        r5.setText("For in that sleep of death what dreams may come");
        r5.addCarriageReturn();
        r5.setText("When we have shuffled off this mortal coil,"
                + "Must give us pause: there's the respect"
                + "That makes calamity of so long life;");
        r5.addBreak();
        r5.setText("For who would bear the whips and scorns of time,"
                + "The oppressor's wrong, the proud man's contumely,");
        
        r5.addBreak(BreakClear.ALL);
        r5.setText("The pangs of despised love, the law's delay,"
                + "The insolence of office and the spurns" + ".......");
		 FileOutputStream out = new FileOutputStream("c:\\workspace_doc\\simple2.docx");
	     doc.write(out);
	     out.close();
		
	}
	
}
