package org.qjin.module;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import org.apache.poi.xwpf.usermodel.XWPFDocument;

import com.argo.hwp.HwpTextExtractor;

public class Main {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		convertHwpToDocVer2 a = new convertHwpToDocVer2();
		XWPFDocument doc = new XWPFDocument();
		
		String[] subPathList = {"01","02","19971998","19981999","19992000"
				,"20002001","2001","2002","20022003","20032004","20042005","20052006","20062007", "20072008"
				,"20082009","20092010","20102011","20112012"};
		//String subPath = "19971998";
		for(String subPath:subPathList){
			String path = "C:\\workspace_hwp\\"+subPath+"\\";
			File dirFile = new File(path);
			File[] fileList = dirFile.listFiles();
			for(File tempFile : fileList){
				String tempPath = tempFile.getParent();
				String tempFileName = tempFile.getName();
				StringBuilder temp = a.convertHwptoDocx(path, subPath, tempFileName);
				if(temp != null){
					a.writeDocToOneFile(doc, temp);
				}
				
				/*if(!a.convertHwptoDocx(path, subPath, tempFileName)){
					System.out.println("Path = "+tempPath);
					System.out.println("FileName =/ "+tempFileName);
				}*/
			}
		}
			
		String filename = "C:\\workspace_doc\\최영기목사님_목회자코너_모음(페이지안바꿈).docx";
		FileOutputStream out = new FileOutputStream(filename);
		doc.write(out);
		out.close();
		
	}

}
