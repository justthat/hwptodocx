/*
 * Copyright (C) 2013 argonet.co.kr <ddoleye@gmail.com>
 * 
 * This library is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * �??�품???��?과컴?�터???��? 문서 ?�일(.hwp) 공개 문서�?참고?�여 개발?��??�니??
 * 
 * �??�품???�음???�스�?참조?��??�니??
 * https://github.com/cogniti/ruby-hwp/
 * https://github.com/cogniti/libghwp/
 */
package com.argo.hwp.v3;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.Arrays;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.argo.hwp.utils.HwpStreamReader;

public abstract class HwpTextExtractorV3 {
	private static Logger log = LoggerFactory
			.getLogger(HwpTextExtractorV3.class);

	// 1byte 문자??.
	private static final byte[] HWP_V3_SIGNATURE = ("HWP Document File V3.00"
			+ " \u001A\u0001\u0002\u0003\u0004\u0005").getBytes();

	public static boolean extractText(File source, Writer writer)
			throws IOException {
		InputStream input = new FileInputStream(source);

		try {
			// ?��?V3 ?�그?�처 ?�인
			try {
				byte[] buf = new byte[HWP_V3_SIGNATURE.length];
				int read = input.read(buf);
				if (read < HWP_V3_SIGNATURE.length)
					return false;

				// ?�그?�처 ?�인
				if (!Arrays.equals(HWP_V3_SIGNATURE, buf))
					return false;
			} catch (IOException e) {
				log.warn("something wrong", e);
				return false;
			}

			extractText(input, writer);

			return true;
		} finally {
			try {
				// from javadoc. If this file has an associated channel then the
				// channel is closed as well.
				input.close();
			} catch (IOException e) {
				log.warn("exception while file.close", e);
			}
		}
	}

	private static void extractText(InputStream inputStream, Writer writer)
			throws IOException {
		// ?�그?�처�??�해??30바이???��? ?�태

		HwpStreamReader input = new HwpStreamReader(inputStream);

		// 문서 ?�보 p.72

		// ?�호 걸린 ?�일 ?�인
		input.ensureSkip(96);
		int t = input.uint16();
		if (t != 0)
			throw new IOException("?�호?�된 문서???�석?????�습?�다");

		// ?�축 ?�인
		input.ensureSkip(26); // 124
		boolean compressed = input.uint8() != 0;
		log.debug("?�축 ?�인 : {}", compressed);

		// ?�보 블럭 길이
		input.ensureSkip(1);
		int blockSize = input.uint16();

		// 문서 ?�약 건너?�기
		input.ensureSkip(1008);
		// ?�보 블럭 건너?�기
		input.ensureSkip(blockSize);

		// ?�축 ??��
		if (compressed)
			input = new HwpStreamReader(new InflaterInputStream(inputStream,
					new Inflater(true)));

		// p.73 �?��?�름 건너?�기
		for (int ii = 0; ii < 7; ii++)
			input.ensureSkip(input.uint16() * 40);

		// p.74 ?��???건너?�기
		input.ensureSkip(input.uint16() * (20 + 31 + 187));

		// <문단 리스?? ::= <문단>+ <빈문??
		// int paraCount = 0;
		while (input.available()) {
			// paraCount++;
			// log.debug("문단 {}", paraCount);
			if (!writeParaText(input, writer))
				break;
		}
	}

	private static boolean writeParaText(HwpStreamReader input, Writer writer)
			throws IOException {
		// # 문단 ?�보
		short prev_paragraph_shape = input.uint8();
		int n_chars = input.uint16();
		int n_lines = input.uint16();
		short char_shape_included = input.uint8();

		// p.77 기�? ?�래그�???.
		input.ensureSkip(1 + 4 + 1 + 31);
		// # ?�기까�? 43 bytes
		if (prev_paragraph_shape == 0 && n_chars > 0)
			input.ensureSkip(187);

		// # 빈문?�이�?false 반환
		if (n_chars == 0) {
			// log.debug("빈문??);
			return false;
		}

		// # �??�보
		input.ensureSkip(n_lines * 14);

		// # �?�� 모양 ?�보 p.78
		if (char_shape_included != 0) {
			for (int ii = 0; ii < n_chars; ii++) {
				short flag = input.uint8();
				if (flag != 1)
					input.ensureSkip(31);
			}
		}

		log.debug("n_chars = {}", n_chars);

		// # �?��??
		int n_chars_read = 0;

		while (n_chars_read < n_chars) {
			int c = input.uint16(); // # 2바이?�씩 ?�는??
			// log.debug("구분 : {}", Integer.toHexString(c));
			n_chars_read++;

			switch (c) {
			case 6: // 책갈??
				n_chars_read += 3;
				input.ensureSkip(6 + 34);
				break;
			case 9: // tab
				n_chars_read += 3;
				input.ensureSkip(6);
				writer.write('\t');
				break;
			case 10: // ??
				n_chars_read += 3;
				input.ensureSkip(6);

				// # ?�이�??�별 ?�보 84 바이??
				input.ensureSkip(80);
				int n_cells = input.uint16();
				input.ensureSkip(2);
				input.ensureSkip(27 * n_cells);

				// # <??문단 리스??+
				for (int ii = 0; ii < n_cells; ii++) {
					// # <??문단 리스?? ::= <??문단>+ <빈문??
					// log.debug("??{}/{}", ii, n_cells);
					while (writeParaText(input, writer))
						;
				}
				// # <캡션 문단 리스?? ::= <캡션 문단>+ <빈문??
				while (writeParaText(input, writer))
					;
				break;

			case 11: // 그림
				n_chars_read += 3;
				input.ensureSkip(6);
				long len = input.uint32();
				input.ensureSkip(344);
				input.ensureSkip(len);
				// # <캡션 문단 리스?? ::= <캡션 문단>+ <빈문??
				while (writeParaText(input, writer))
					;
				break;
			case 13: // # �?��????
				writer.write('\n');
				break;
			case 16: // # 머리�?꼬리�?
				n_chars_read += 3;
				input.ensureSkip(6);
				input.ensureSkip(10);

				// # <문단 리스?? ::= <문단>+ <빈문??
				while (writeParaText(input, writer))
					;
				break;

			case 17: // # 각주/미주
				n_chars_read += 3;
				input.ensureSkip(6);
				// # 각주/미주 ?�보 건너 ?�기
				input.ensureSkip(14);
				while (writeParaText(input, writer))
					;
				break;
			case 18:
			case 19:
			case 20:
			case 21:
				n_chars_read += 3;
				input.ensureSkip(6);
				break;
			case 23: // # �?�� 겹침
				n_chars_read += 4;
				input.ensureSkip(8);
				break;
			case 24:
			case 25:
				n_chars_read += 2;
				input.ensureSkip(4);
				break;
			case 28: // # 개요 모양/번호
				n_chars_read += 31;
				input.ensureSkip(62);
				break;
			case 30:
			case 31:
				n_chars_read += 1;
				input.ensureSkip(2);
				break;
			default:
				if (c >= 0x0020 && c <= 0xffff) {// # hnc code range
					String s = Hnc2String.convert(c);
					if (s == null) {
						log.warn("매핑 문자 ?�음 {}", Integer.toHexString(c));
						writer.write(unknown(c));
					} else {
						writer.write(s);
					}
				} else {
					log.error("?�수 문자 ? : {}", Integer.toHexString(c));
					// throw new NotImplementedException();
				}
			}
		}

		return true;
	}

	private static String unknown(int c) {
		return String.format("?+0x%1$04x", c);
	}
}
